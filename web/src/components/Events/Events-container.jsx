import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import List from "@material-ui/core/List";

import ListItems from "../ListItems/ListItems";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  }
}));

const MsgList = [];

const EventsContainer = props => {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      <ListItems type="ListItemText" data={MsgList} />
    </List>
  );
};

export default EventsContainer;
