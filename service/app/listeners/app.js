const { log } = require('../shared/functions/log');

class AppListener {

    constructor(app) {
        app.on('afterBootstrap', this.afterBootstrap);
        app.on('beforeBootstrap', this.beforeBootstrap);
    }

    beforeBootstrap() {

    }

    afterBootstrap() {

    }


}

module.exports = { AppListener };