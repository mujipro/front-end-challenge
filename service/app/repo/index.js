const { AgentRepo } = require('./agent');
const { CallRepo } = require('./call');
const { LogsRepo } = require('./logs');


module.exports = {
    AgentRepo,
    CallRepo,
    LogsRepo
};