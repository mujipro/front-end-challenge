'use strict';
const httpError = require('http-errors');
const _ = require('lodash');
const { log } = require('./log');
const { config } = require('../../../config');
const { debugging } = config.get('misc');

const ERROR_CODES = {
    "TASK500001": { status: 500, code: "TASK500001", title: "Internal Server Error", message: "Something went wrong." },
    "TASK422001": { status: 422, code: "TASK422001", title: "Validation Error", message: "Validation Error" },
    "TASK422002": { status: 422, code: "TASK422002", title: "Record Not Found", message: "Record Not Found" },
    "TASK422003": { status: 422, code: "TASK422003", title: "Failed to Insert Record", message: "Failed to Insert Record" },
    "TASK422004": { status: 422, code: "TASK422004", title: "Failed to Update Record", message: "Failed to Insert Record" },
    "TASK422005": { status: 422, code: "TASK422005", title: "Failed to Delete Record", message: "Failed to Insert Record" },
    "TASK422006": { status: 422, code: "TASK422006", title: "Record Already Exists", message: "Record Already Exists" },
    "TASK422007": { status: 422, code: "TASK422007", title: "Unknown Data Exception", message: "Unknown Data Exception" },
    "TASK422008": { status: 422, code: "TASK422001", title: "Validation Error", message: "It is being Use" },
    "TASK403001": { status: 403, code: "TASK403001", title: "Forbidden", message: "Access is Denied" },
    "TASK404001": { status: 404, code: "TASK404001", title: "Not Found", message: "Requested API Not Found" },
    "TASK404002": { status: 404, code: "TASK404002", title: "Email Not Found", message: "Email Not Found" },
    "TASK404003": { status: 404, code: "TASK404003", title: "Forbidden", message: "Your token has expired" },
    "TASK404004": { status: 404, code: "TASK404003", title: "Not Found", message: "Credentials are invalid" },
};


const createError = (req, res, next, code, err) => {
    try {

        let error = code;

        if (typeof err == 'string') {
            console.log(err);
            error.message = err;
        } else if (err && debugging) {

            error.message = err.message;
            error.title = err.name;
            error.stack = err.stack;

            if (err.name.indexOf('Sequelize') > -1) { //For Example: 'SequelizeUniqueConstraintError'
                error.title = "Validation Error";

                if (err.errors) {
                    for (let e of err.errors) {
                        error.message += `${e.message}\n`;
                    }
                    error.message = err.errors[0] ? err.errors[0].message : err.message;
                }
            }
        }

        //log error async
        new Promise((resolve, reject) => {
            log.Info(req.url+":errorHandler", "errorHandler", "errorHandler", error);
            resolve(1);
        });

      /*  if (debugging) {
            return error;
        } else {
            return code;
        }*/

        return error;
    } catch (err) {
        log.Error("File - create-error", "Internal Service", "createError", err.message);
        return next(httpError(500, err))
    }
};

const logError = (code, err) => {
    let error = code;


    if (typeof err === 'string') {
        error.message = err;
    } else {
        error.message = err.message;
        error.title = err.name;
        error.stack = err.stack;
    }

    log.Info("errorHandler", "errorHandler", "errorHandler", error);
};


module.exports = { ERROR_CODES, createError, logError };
