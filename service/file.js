module.exports = {
    path: "assets/json-data",
    resolutions: "resolution.json",
    agents: "agents.json",
    logs: "logs.json",
}
