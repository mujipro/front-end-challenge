import AgentsPage from "./AgentsPage";

export const AgentsPageConfig = {
  routes: [
    {
      path: "/agent/:identifier",
      exact: true,
      component: AgentsPage
    }
  ]
};
