import React from "react";
import Layout from "../../components/Layout/Layout";
import {Grid, makeStyles, Paper} from "@material-ui/core";
import Breadcrumbs from "../../components/Breadcrumbs/Breadcrumbs";
import SimpleTable from "./components/SimpleTable";
import clsx from "clsx";

import {ArrowBack} from '@material-ui/icons';

const useStyles = makeStyles((them) => ({
  paddingPaper: {
    padding: "10px 5px 5px 10px",
  },
  mt: {
    marginTop: 13,
  },
  titlePaper: {
    marginBottom: "16px",
  },
  visitorChart: {
    // height: "150px"
  },
}));

const AgentsPage = (props) => {
  const { history } = props;
  const classes = useStyles();

  return (
    <Layout>
      <ArrowBack onClick={()=>{
        history.push('/');
      }}/>
      <h1>Agents</h1>
      <Breadcrumbs path={history} />
        <Grid item container xs={12} sm={12}>
          <Grid item xs={12}>
            <Paper
              className={clsx(classes.paddingPaper, classes.mt)}
              variant="outlined"
            >
              <SimpleTable args={props} />
            </Paper>
          </Grid>
        </Grid>
    </Layout>
  );
};

export default AgentsPage;
