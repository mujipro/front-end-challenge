const { ERROR_CODES, createError } = require('./create-error');
const _ = require('lodash');

const authenticate = async (req, res, next) => {
    try {
        next();
    } catch (err) {

        console.log(err);
        return next(createError(req, res, next, ERROR_CODES.BUZZ403001, err))
    }
};

module.exports = { authenticate };