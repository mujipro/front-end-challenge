const express = require("express");
const morgan = require("morgan");
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const { initializeRoutes } = require('./routes');
const { db, initialize } = require('./models');
const { accessControl, notfound, error } = require('./middlewares');
const { log } = require('./shared/functions/log');
const { AppListener } = require('./listeners');
const util = require('util');
const http = require('http');
const EventEmitter = require('events');
const io = require('socket.io');
const helmet = require('helmet');



class App extends EventEmitter {

    constructor(configuration) {
        super();
        this.app = express();
        this.http = http.createServer(this.app);

        this.io = io(this.http);

        this.router = express.Router();
        this.conf = configuration;

        new AppListener(this);
        //new ProcessListener(process);


    }

    async bootstrap() {


        await this.syncDatabase();

        initializeRoutes(this.router);

        this.app.options('*', accessControl);

        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'pug');

        this.app.use(helmet());
        this.app.use(accessControl);

        this.app.use(morgan());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cookieParser());


        this.app.use(express.static(path.join(__dirname, '../public')));
        this.app.use(express.static(path.join(__dirname, '../storage')));
        this.app.use(express.static(path.join(__dirname, '../assets')));

        this.app.use(this.router);


        this.app.use(notfound);
        this.app.use(error);

        this.emit('afterBootstrap');

        return Promise.resolve();

    }

    async syncDatabase() {
        return Promise.resolve(1);
    }

    get() {
        return this.app;
    }

    start(cb) {
        console.log(`starting server on port ${this.conf.get('port')}`);
        this.http.listen(this.conf.get('port'), cb);
    }



}

module.exports = { App };