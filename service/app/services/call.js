const { CallRepo } = require('../repo');
const { CallNotFound } = require('../errors');

class CallService {
    constructor() {
        this.callRepo = new CallRepo();
    }


    async getAll(type) {
        return this.callRepo.getAll(type)
    }

    get(id) {
        return this.callRepo.get(id)
    }


}

module.exports = { CallService };
