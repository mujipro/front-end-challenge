const { config } = require("./../../config");


const { error_type } = config.get('misc');
const env = config.get('env');

const notfound =  (req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;

    res.status(err.status);
    res.render('404', {
        message: err.message,
        error: (env === 'development') ? err : {}
    });
};

module.exports = { notfound };