const EventEmitter = require('events');
const { CallService } = require('./../services');
const _ = require("lodash");
const Constants = require("../shared/constants");
const { ERROR_CODES, createError } = require('../shared/functions/create-error');
const { CallNotFound } = require('../errors');
const Sequelize = require('sequelize');
const { config } = require('../../config');
const { server_url } = config.get('misc');

class CallController extends EventEmitter {
    constructor() {
        super();
        this.callService = new CallService();
    }

    async getCalls(req, res, next) {

        try {

            let result = await this.callService.getAll('resolutions');

            let json = {};

            if (result) {
                json.success = true;
                json.data = result
            }

            if (_.isEmpty(json)) {
                json.error = true;
                json.message = "Not Found";
            }

            return res.json(json);
        } catch (ex) {
            next(createError(req, res, next, getErrorCode(ex), ex))
        }
    }


    async getCallByNumber(req, res, next) {

        try {

            let number = req.params.number;
            if(number){
                number = "+" + number;
            }

            let result = await this.callService.get(number);

            let json = {};

            if (result) {
                json.success = true;
                json.data = result
            }

            if (_.isEmpty(json)) {
                json.error = true;
                json.message = "Not Found";
            }

            return res.json(json);
        } catch (ex) {
            next(createError(req, res, next, getErrorCode(ex), ex))
        }
    }
}

const getErrorCode = (ex) => {
    let code = ERROR_CODES.TASK500001;

    switch (ex.constructor) {
        case CallNotFound:
            code = ERROR_CODES.TASK404001;
            break;
        default:
            code = ERROR_CODES.TASK500001;
    }

    return code;
};


module.exports = { CallController };