const Joi = require('@hapi/joi');
const { validate } = require('../shared/functions/validate');


const validationCriteria = {
    default: {
        GET: Joi.object({
            number: Joi.string().required(),
        }),
    },

};

const callValidation = (resource) => {
    return (req, res, next) => {

        validate(req, res, next, validationCriteria[resource][req.method]);
    }
};


module.exports = { callValidation };
