const { AgentController } = require('./../controller');
const { agentValidation } = require('./../validations');
const { AgentListener } = require('./../listeners');
const { authenticate,authorize } = require("../shared/functions");

class AgentRouter {

    constructor(router) {
        this.agentController = new AgentController();
        this.agentListener = new AgentListener(this.agentController);
        this.router = router;
        this.registerRoutes();

    }
    registerRoutes() {
        this.router.get("/api/v1/agents/", authenticate,this.agentController.getAgents.bind(this.agentController));
        this.router.get("/api/v1/agent/:identifier", authenticate,agentValidation('default') ,this.agentController.getAgent.bind(this.agentController));
        this.router.get("/api/v1/agents/agentCalls", authenticate ,this.agentController.getAgentCalls.bind(this.agentController));
    }

}


module.exports = { AgentRouter };