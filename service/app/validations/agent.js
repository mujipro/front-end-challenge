const Joi = require('@hapi/joi');
const { validate } = require('../shared/functions/validate');


const validationCriteria = {
    default: {
        GET: Joi.object({
            identifier: Joi.string().required(),
        }),
    },

};

const agentValidation = (resource) => {
    return (req, res, next) => {

        validate(req, res, next, validationCriteria[resource][req.method]);
    }
};


module.exports = { agentValidation };
