const {readFile} = require("../shared/functions");


class CallRepo {

    constructor() {

    }

    getAll(type) {
        return readFile(type);
    }


    get(id) {
        try {
            const logs = readFile('logs');
            const agents = readFile('agents');
            const resolution = readFile('resolutions');
            const result = [];
            logs.forEach(element => {
                if (element.number === id) {
                    const agent = agents[element.agentIdentifier]
                    const date =  new Date(element.dateTime);
                    result.push({
                        name: agent.firstName + " " + agent.lastName,
                        resolution: resolution[element.identifier],
                        dateTime: date.toLocaleString()
                    })
                }
            });
            return result;

        } catch (e) {
            throw (e);
        }
    }

}

module.exports = { CallRepo };
