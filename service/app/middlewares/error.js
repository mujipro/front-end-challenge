const { config } = require("./../../config");


const { error_type } = config.get('misc');
const env = config.get('env');

const error = (err, req, res, next) => {

    res.status(err.status || 500);
    if (error_type === 'json') {
        res.json({
            error: err.title,
            message: err.message,
        });
    } else {
        res.render('error', {
            message: err.message,
            error: (env === 'development') ? err : {}
        });

    }
};


module.exports = { error };