import React from "react";
import {Redirect} from "react-router-dom";
import {DashboardPageConfig} from "../pages/dashboard/DashboardPageConfig";
import {Error404PageConfig} from "../pages/errors/404/Error404PageConfig";
import {Error500PageConfig} from "../pages/errors/500/Error500PageConfig";
import {CallsPageConfig} from "../pages/calls/CallsPageConfig";
import {AgentsPageConfig} from "../pages/agents/AgentsPageConfig";

const routeConfigs = [
  ...DashboardPageConfig.routes,
  ...CallsPageConfig.routes,
  ...AgentsPageConfig.routes,
  ...Error404PageConfig.routes,
  ...Error500PageConfig.routes,
];

const routes = [
  ...routeConfigs,
  {
    component: () => <Redirect to="/pages/errors/error-404" />
  }
];

export default routes;
