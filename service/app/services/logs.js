const { LogsRepo } = require('../repo');
const { LogsNotFound } = require('../errors');

class LogsService {
    constructor() {
        this.logsRepo = new LogsRepo();
    }


    async getAll(type) {
       return this.logsRepo.getAll(type)
    }

    get(id) {

    }


}

module.exports = { LogsService };
