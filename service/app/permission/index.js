module.exports.User = [
    { role: 'user', resource: 'agents', action: 'read:any', attributes: '*' },
    { role: 'user', resource: 'agents', action: 'update:any', attributes: '*' },
    { role: 'user', resource: 'agents', action: 'read:any', attributes: '*' },
    { role: 'user', resource: 'agents', action: 'create:any', attributes: '*' },
    { role: 'user', resource: 'agents', action: 'delete:any', attributes: '*' },

    { role: 'user', resource: 'calls', action: 'read:any', attributes: '*' },
    { role: 'user', resource: 'calls', action: 'update:any', attributes: '*' },
    { role: 'user', resource: 'calls', action: 'read:any', attributes: '*' },
    { role: 'user', resource: 'calls', action: 'create:any', attributes: '*' },
    { role: 'user', resource: 'calls', action: 'delete:any', attributes: '*' },

];
