import IndexService from "../services/index.service";

export const getDashboardReading = async () => {
    const service = new IndexService();
    let dashboard = {
        numberOfCall: 0,
        totalDuration: 0,
        user: null
    };

    await service.getLogs().then((res)=>{
           if(res){
               res.data.map((r)=>{
                   dashboard.numberOfCall += r.numberOfCall;
                   dashboard.totalDuration += r.duration;
               })

               dashboard.user = res.data.reduce(function(prev, curr) {
                   return prev.numberOfCall > curr.numberOfCall ? prev : curr;
               });

           }
    });

    return dashboard;
}

