const accessControl = (req, res, next) => {

    res.header("Access-Control-Allow-Credentials", "true");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, OPTIONS, PUT, POST, PATCH,DELETE");
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Max-Age", "3600");
    res.header("Access-Control-Allow-Headers", "Authorization,Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");


    /*res.header("Access-Control-Allow-Origin", "*");
     res.header("Access-Control-Allow-Methods", "GET, OPTIONS, PUT, POST");
     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");*/
    next();
};

module.exports = { accessControl };


