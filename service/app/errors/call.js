class CallNotFound extends Error {
    constructor(...args) { super(args) }
}

module.exports = {
    CallNotFound
};
