import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import List from "@material-ui/core/List";

import ListItems from "../ListItems/ListItems";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  }
}));

const notifList = [];

const NotificationsContainer = props => {
  const classes = useStyles();

  return (
    <List className={classes.root}>
      <ListItems type="ListItemIcon" data={notifList} />
    </List>
  );
};

export default NotificationsContainer;
