// server.js
const dotenv = require('dotenv').config();

const { App } = require("./app");
const { config } = require("./config");



String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};


const app = new App(config);

app.bootstrap().then(() => app.start());

// 1. require app
// 2. bootstrap
// 3. start the application
