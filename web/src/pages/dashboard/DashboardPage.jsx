import React, {useEffect, useState} from "react";
import Layout from "../../components/Layout/Layout";
import {Card, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import Breadcrumbs from "../../components/Breadcrumbs/Breadcrumbs";
import Chart from "../../components/Chart/Chart";
import SimpleTable from "./components/SimpleTable";
import clsx from "clsx";
import {AccessAlarmRounded, AccountBalanceOutlined, CallRounded} from "@material-ui/icons";
import {getDashboardReading} from "../../utils/index";


const useStyles = makeStyles((them) => ({
  paddingPaper: {
    padding: "10px 5px 5px 10px",
  },
  mt: {
    marginTop: 13,
  },
  titlePaper: {
    marginBottom: "16px",
  },
  visitorChart: {
    // height: "150px"
  },

  root: {
    width: '100%',
    padding: 10
  },

  icon: {
    width: 45,
    height: 45,
    borderRadius: 22,
    background: '#27344c',
    display: "flex",
    justifyContent: "center",
    alignItems:"center"
  },
  inline : {
    alignItems: "center",
    display: "flex",
    justifyContent: "center"
  },
  col: {
    display: "flex",
    flexDirection: "column",
    marginLeft: 20
  },
  heading: {
    fontWeight: 'bold',
    fontSize: 16
  }
}));

const DashboardPage = (props) => {
  const { history } = props;
  const classes = useStyles();
  const [call,setCalls] = useState(0);
  const [duration,setDuration] = useState(0);
  const [user,setUser] = useState(null);

  useEffect( ()=>{
         getReadings();
  },[])

  const getReadings = async  () => {
    const dashboard = await getDashboardReading();
    setCalls(dashboard.numberOfCall);
    setDuration(dashboard.totalDuration);
    setUser(dashboard.user);
  }

  return (
    <Layout>
      <h1>Dashboard</h1>
      <Breadcrumbs path={history} />
      <Grid container spacing={2}>
        <Grid className={classes.visitorChart} item xs={12}>
          <Paper className={classes.paddingPaper} variant="outlined">
            <Typography className={classes.titlePaper} variant="h5">
              Visitors
            </Typography>
            <Chart />
          </Paper>
        </Grid>
        <Grid item container xs={12} sm={12}>
          <Card  className={classes.root}>
            <Grid container>
            <Grid item xs={4} className={classes.inline}>
              <div className={classes.icon}>
               <CallRounded style={{color: "white"}} />
               </div>
              <div className={classes.col}>
               <span className={classes.heading}>&nbsp;Calls Attended</span>
               <span>&nbsp;{call}</span>
              </div>
            </Grid>
            <Grid item xs={4} className={classes.inline}>
               <div className={classes.icon}>
              <AccessAlarmRounded style={{color: "white"}}  />
               </div>
              <div className={classes.col}>
              <span className={classes.heading}>&nbsp;Duration Covered</span>
              <span>&nbsp;{duration}</span>
              </div>
            </Grid>
            <Grid item xs={4} className={classes.inline}>
               <div className={classes.icon}>
              <AccountBalanceOutlined style={{color: "white"}} />
               </div>
              <div className={classes.col}>
              <span className={classes.heading}>&nbsp;Highest Call</span>
              <span>&nbsp;{user?.name} - &nbsp;{user?.numberOfCall}</span>
              </div>
            </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item container xs={12} sm={12}>
          <Grid item xs={12}>
            <Paper
              className={clsx(classes.paddingPaper, classes.mt)}
              variant="outlined"
            >
              <SimpleTable history={history} />
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default DashboardPage;
