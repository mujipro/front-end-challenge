import CallsPage from "./CallsPage";

export const CallsPageConfig = {
  routes: [
    {
      path: "/call/:number",
      exact: true,
      component: CallsPage
    }
  ]
};
