const { AgentRouter } = require('./agent');
const { CallRouter } = require('./call');
const { LogsRouter } = require('./logs');

const initializeRoutes = (router) => {
    new AgentRouter(router);
    new CallRouter(router);
    new LogsRouter(router);
};

module.exports = { initializeRoutes };