const { LogsController } = require('./../controller');
const { LogsListener } = require('./../listeners');
const { authenticate } = require("../shared/functions");

class LogsRouter {

    constructor(router) {


        this.logsController = new LogsController();
        this.logsListener = new LogsListener(this.logsController);


        this.router = router;

        this.registerRoutes();


    }

    registerRoutes() {
        this.router.get("/api/v1/logs/", authenticate,this.logsController.getLogs.bind(this.logsController));
    }

}


module.exports = { LogsRouter };