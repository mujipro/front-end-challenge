import axios from "axios";


export default class IndexService {

    constructor() {
        this.api = axios.create({
            baseURL: 'http://localhost:3000/api/v1'
        });
    }


    async getLogs() {
        return await this.api({
            method: 'get',
            url: '/logs'
        }).then((res) => res.data);
    }

    async getAllCalls() {
        return await this.api({
            method: 'get',
            url: '/calls'
        }).then((res) => res.data);
    }

    async getCallByNumber(number) {
        return await this.api({
            method: 'get',
            url: '/call/' + number
        }).then((res) => res.data);
    }

    async getAgentByIdentifier(identifier) {
        return await this.api({
            method: 'get',
            url: '/agent/' + identifier
        }).then((res) => res.data);
    }

    async getAllAgents(){
        return await this.api({
            method: 'get',
            url: '/agents',
        }).then((res) => res.data);
    }


    async getAgentChart(){
        return await this.api({
            method: 'get',
            url: '/agents/agentCalls',
        }).then((res) => res.data);
    }

}
