'use strict';

const { ERROR_CODES, createError } = require('./create-error');


const formParser = (req, res, next) => {
    try {
        if (req.method != 'GET') {

            req.body = JSON.stringify(req.body);

            req.body = JSON.parse(req.body);

            next();
        } else {
            return next(createError(req, res, next, ERROR_CODES.BUZZ500001, 'Request Method is Invalid - It Should not be GET'))
        }
    } catch (err) {
        return next(createError(req, res, next, ERROR_CODES.BUZZ500001, err))
    }
};

module.exports = { formParser };
