
#Project Buzz
##Node Version
v10.15.3

### Api Route Practice
* routes have format */api/v1/resource-name/*
* http verb are used to distinguish operations Like GET | POST | PUT | DELETE
* routes containing  */b/*  means backend resource
* routes containing  */f/*  means frontend resource
* routes not containing any of the above means resource is for both backend and frontend
 


##Angular
Global : v8.0.0
Local : 7.3.9

##Sequelize

####Creating Migration 
 
npx sequelize-cli migration:generate --name migration-skeleton

####Creating Seed 

npx sequelize-cli seed:generate --name migration-skeleton




About API
 
###Project Complexity And Estimation

minimum : 2 hours

Procedure
- Get an understanding of what to make : 30% time
- Development 50% time
- Testing 20% time
Example
2 hours division : understanding (36min) + development (60min) + Testing (24)

Complexity Level: 1
1 table

id name is_active, date_added, date_updated

4 API & Testing
    create, read, update, delete
Estimated : 2


Complexity Level: 2
includes Following
- calculation
Estimated : 4

Complexity Level: 3
includes Following
- calculation
- Sequelize Transaction
Estimated : 6


Complexity Level: 4
includes Following
- calculation
- Sequelize Transaction
- Testing
Estimated : 9

