const { log } = require('../shared/functions/log');

class CallListener {

    constructor(controller) {
        controller.on('service', ()=>{});
        controller.on('beforeBootstrap', ()=>{});
    }


}

module.exports = { CallListener };