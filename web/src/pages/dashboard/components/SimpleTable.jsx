import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import IndexService from "../../../services/index.service";
import {DataGrid} from "@material-ui/data-grid";
import TableContainer from "@material-ui/core/TableContainer";
import PhoneIcon from '@material-ui/icons/Phone';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles({
  table: {
//    minWidth: 650,
  },
  number: {
    margin: "auto",
    width: 180,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: '#075e54',
    color: "white",
    borderRadius: 30,
    height: 40,
    minWidth: 60,
    cursor: "pointer"
  },
  user: {
    margin: "auto",
    width: 180,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: '#833AB4',
    color: "white",
    borderRadius: 30,
    height: 40,
    minWidth: 60,
    cursor: "pointer"
  },
});


const SimpleTable = (props) => {

  const {history} = props;
  const service = new IndexService();
  const [data,setData] = useState([]);

  const columns = [
    { field: 'id', headerName: 'ID' , width: 120,flex: 0.25,    headerAlign: 'center',},
    { field: 'number', headerName: 'Phone Number', width: 180, flex: 0.25 ,
      headerAlign: 'center',
      renderCell: (params) => {
        return <><span
            onClick={()=>{
              history.push('/call/' + params.row.number)
            }}
            className={classes.number}><PhoneIcon /> &nbsp; {params.formattedValue}</span></>;
      }},
    { field: 'numberOfCall', headerName: 'Number Of Call', flex: 0.25,    headerAlign: 'center',   },
    { field: 'agent', headerName: 'Last call details', flex: 0.25 ,
      headerAlign: 'center',
      renderCell: (params) => {
        return <><span
            onClick={()=>{
              history.push('/agent/' + params.row.identifier)
            }}
            className={classes.user}><AccountCircleIcon /> &nbsp; {params.formattedValue}</span></>;
      }
    },
  ];


  useEffect(()=>{
    getLogs();
  },[])

  const getLogs = () => {
    service.getLogs().then((res)=>{
      const data = aggregateArray(res.data);
      data.map((d,index)=>{
        d['id'] = index + 1;
        d['agent'] = d['name'] + ' / ' + secondsToDhms(d['duration']);
      })
      setData(data);
    }).catch((err)=>{

    });
  }

  function aggregateArray(arr){
    return arr.reduce((ac,a) => {
      let ind = ac.findIndex(x => x.number === a.number);
      ind === -1 ? ac.push(a) : ac[ind].duration += a.duration;
      return ac;
    },[])
  }

  function secondsToDhms(seconds) {
    seconds = Number(seconds);
    let d = Math.floor(seconds / (3600*24));
    let h = Math.floor(seconds % (3600*24) / 3600);
    let m = Math.floor(seconds % 3600 / 60);
    let s = Math.floor(seconds % 60);

    let dDisplay = d > 0 ? (d < 10 ? '0' + d : d) + ":" : "";
    let hDisplay = h > 0 ? (h < 10 ? '0' + h : h) + ":" : "";
    let mDisplay = m > 0 ? (m < 10 ? '0' + m : m)  + ":" : "";
    let sDisplay = s > 0 ? (s < 10 ? '0' + s : s) + "" : "";
    return dDisplay + hDisplay + mDisplay + sDisplay;
  }


  const classes = useStyles();





  return (
    <TableContainer style={{ height: 400, width: '100%' }}>
      <div style={{ display: 'flex', height: '100%' }}>
        <div style={{ flexGrow: 1 }}>
      {data && data.length > 0 ? <DataGrid
          dataSet='Commodity'
          className={classes.table}  rows={data} columns={columns} pageSize={5}  />: null}
        </div>
      </div>
    </TableContainer>
  );
};

export default SimpleTable;
