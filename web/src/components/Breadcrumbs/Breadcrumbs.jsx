import React from "react";
import {Breadcrumbs, Link, makeStyles, Typography} from "@material-ui/core";
import NavLinkAdapter from "../../components/NavLinkAdapter/NavLinkAdapter";

const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: 20
  }
}));

const Breadcrumb = props => {
  const { path } = props;
  const classes = useStyles();

  const pathName = path.location.pathname.split("/");
  const lastRoute = pathName.splice(-1, 1);


  function pascal(string) {
    return `${string}`
        .replace(new RegExp(/[-_]+/, 'g'), ' ')
        .replace(new RegExp(/[^\w\s]/, 'g'), '')
        .replace(
            new RegExp(/\s+(.)(\w+)/, 'g'),
            ($1, $2, $3) => `${$2.toUpperCase() + $3.toLowerCase()}`
        )
        .replace(new RegExp(/\s/, 'g'), '')
        .replace(new RegExp(/\w/), s => s.toUpperCase());
  }

  return (
    <div className={classes.root}>
      <Breadcrumbs aria-label="breadcrumb">
        {pathName.length > 1 &&
          pathName.map((item, index) => (
            <Link
              className={"disabled"}
              key={index}
              component={NavLinkAdapter}
              to={pathName.join("/")}
              activeClassName="active"
              exact={true}
              color="inherit"
            >
              {item === "" ? "Dashboard" : pascal(item)}
            </Link>
          ))}
        <Typography color="textPrimary">{lastRoute.toString()}</Typography>
      </Breadcrumbs>
    </div>
  );
};

export default Breadcrumb;
