const Joi = require('@hapi/joi');
const { validate } = require('../shared/functions/validate');


const validationCriteria = {


};

const logsValidation = (resource) => {
    return (req, res, next) => {

        validate(req, res, next, validationCriteria[resource][req.method]);
    }
};


module.exports = { logsValidation };
