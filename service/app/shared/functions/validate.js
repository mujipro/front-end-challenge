const Joi = require('@hapi/joi');
const { ERROR_CODES, createError } = require('./create-error');
const _ = require('lodash');

const validate = (req, res, next, criteria) => {
    try {

        let data;
        data = { ...req.body, ...req.query, ...req.params };

        let validation = criteria.validate(data);
        if (validation.error) {

            return next(createError(req, res, next, ERROR_CODES.TASK422001, validation.error.details[0].message))
        } else {
            return next()
        }
    } catch (err) {
        console.log("ERROR:functions/validate.js",err);
        return next(createError(req, res, next, ERROR_CODES.TASK500001, err))
    }
};

module.exports = { validate };