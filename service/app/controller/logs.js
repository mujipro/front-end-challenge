const {LogsService, AgentService} = require('./../services');
const {ERROR_CODES, createError} = require('../shared/functions/create-error');
const {LogsNotFound} = require('../errors');
const EventEmitter = require('events');

const {config} = require('../../config');
const _ = require('lodash');



class LogsController extends EventEmitter {

    constructor() {
        super();
        this.logsService = new LogsService();
        this.agentService = new AgentService();
    }

    async getLogs(req, res, next) {

        try {


            let result = await this.logsService.getAll('logs');
            let logs = result;
            let customData = {}
            logs.forEach(element => {
                if (!customData[element.number]) {
                    customData[element.number] = [element]
                } else {
                    customData[element.number].push(element)
                }
            });

            let agent = await this.agentService.getAll('agents');

            let agents = [];

            for (const number in customData) {
                const object = customData[number];
                const last = object[object.length - 1];
                const _agent = agent[last.agentIdentifier];

                const data = {
                    identifier: _agent.identifier,
                    number,
                    numberOfCall: object.length,
                    name: _agent.firstName + " " + _agent.lastName,
                    duration: last.duration,
                };
                agents.push(data);
            }

            let json = {};

            if (result) {
                json.success = true;
                json.data = agents
            }

            if (_.isEmpty(json)) {
                json.error = true;
                json.message = "Not Found";
            }

            return res.json(json);
        } catch (ex) {
            console.log("ex",ex);
            next(createError(req, res, next, getErrorCode(ex), ex))
        }
    }


}

const getErrorCode = (ex) => {
    let code = ERROR_CODES.TASK500001;

    switch (ex.constructor) {
        case LogsNotFound:
            code = ERROR_CODES.TASK404001;
            break;
        default:
            code = ERROR_CODES.TASK500001;
    }

    return code;
};


module.exports = {LogsController};