const { AgentService } = require('./../services');
const { ERROR_CODES, createError } = require('../shared/functions/create-error');
const { AgentNotFound } = require('../errors');
const EventEmitter = require('events');

const _ = require('lodash');


class AgentController extends EventEmitter {

    constructor() {
        super();
        this.agentService = new AgentService();
    }

    async getAgents(req, res, next) {

        try {


            let result = await this.agentService.getAll('agents');
            console.log(result);
            let json = {};

            if (result) {
                json.success = true;
                json.data = result
            }

            if (_.isEmpty(json)) {
                json.error = true;
                json.message = "Not Found";
            }

            return res.json(json);
        } catch (ex) {
            next(createError(req, res, next, getErrorCode(ex), ex))
        }
    }


    async getAgent(req, res, next) {

        try {

            const identifier  = req.params.identifier;

            let result = await this.agentService.get(identifier);

            let json = {};

            if (result) {
                json.success = true;
                json.data = result
            }

            if (_.isEmpty(json)) {
                json.error = true;
                json.message = "Not Found";
            }

            return res.json(json);
        } catch (ex) {
            next(createError(req, res, next, getErrorCode(ex), ex))
        }
    }


    async getAgentCalls(req, res, next) {

        try {


            let result = await this.agentService.getAgentByCall();

            let json = {};

            if (result) {
                json.success = true;
                json.data = result
            }

            if (_.isEmpty(json)) {
                json.error = true;
                json.message = "Not Found";
            }

            return res.json(json);
        } catch (ex) {
            next(createError(req, res, next, getErrorCode(ex), ex))
        }
    }


}

const getErrorCode = (ex) => {
    let code = ERROR_CODES.TASK500001;

    switch (ex.constructor) {
        case AgentNotFound:
            code = ERROR_CODES.TASK404001;
            break;
        default:
            code = ERROR_CODES.TASK500001;
    }

    return code;
};



module.exports = { AgentController };