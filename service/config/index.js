const convict = require('convict');
const fs = require('fs');


const config = convict({
    env: {
        doc: "The application environment.",
        format: ["production", "development", "qa", "stage", "test"],
        default: "development",
        env: "NODE_ENV"
    },
    port: {
        doc: "The port to bind.",
        format: "port",
        default: 3000,
        env: "PORT"
    },
    mail: {
    },
    sms: {

    },
    db: {
    },
    misc: {

    }
});

const env = config.get("env");
try {
    const path = `${__dirname}/${env}.json`;
    console.log("trying to access %s", path);
    fs.accessSync(path, fs.constants.F_OK);

    config.loadFile(path);
} catch (error) {
    console.error(error);
    console.error("file doesn't exist, loading defaults");
}

config.validate({ allowed: "strict" });

module.exports = { config };

