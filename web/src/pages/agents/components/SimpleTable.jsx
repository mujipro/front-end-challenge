import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import IndexService from "../../../services/index.service";
import {DataGrid} from "@material-ui/data-grid";
import TableContainer from "@material-ui/core/TableContainer";

const useStyles = makeStyles({
  table: {
//    minWidth: 650,
  },
  number: {
    margin: "auto",
    width: 180,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: '#075e54',
    color: "white",
    borderRadius: 30,
    height: 40,
    minWidth: 60,
    cursor: "pointer"
  },
  user: {
    margin: "auto",
    width: 180,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: '#833AB4',
    color: "white",
    borderRadius: 30,
    height: 40,
    minWidth: 60,
    cursor: "pointer"
  },
});


const SimpleTable = (props) => {
  const service = new IndexService();
  const [data,setData] = useState([]);

  const columns = [
    { field: 'id', headerName: 'ID' , width: 120,flex: 0.25,    headerAlign: 'center',},
    {
      field: 'number', headerName: 'Phone Number', width: 180, flex: 0.25,
      headerAlign: 'center'
    },
    {
      field: 'dateTime', headerName: 'Call date and time', width: 180, flex: 0.25,
      headerAlign: 'center'
    },
    { field: 'resolution', headerName: 'Resolution', flex: 0.25,    headerAlign: 'center',   },
  ];


  useEffect(()=>{
    if(props?.args?.match?.params?.identifier) {
      getAgentByIdentfier();
    }
  },[])

  const getAgentByIdentfier = () => {
    service.getAgentByIdentifier(props?.args?.match?.params?.identifier).then((res)=>{
      res.data.map((d,index)=>{
        d['id'] = index + 1;
      })
      setData(res.data);
    }).catch((err)=>{

    });
  }

  const classes = useStyles();

  return (
    <TableContainer style={{ height: 400, width: '100%' }}>
      <div style={{ display: 'flex', height: '100%' }}>
        <div style={{ flexGrow: 1 }}>
      {data && data.length > 0 ? <DataGrid
          dataSet='Commodity'
          className={classes.table}  rows={data} columns={columns} pageSize={5}  />: null}
        </div>
      </div>
    </TableContainer>
  );
};

export default SimpleTable;
