const { log } = require('../shared/functions/log');

class LogsListener {

    constructor(controller) {
        controller.on('service', ()=>{});
        controller.on('beforeBootstrap', ()=>{});
    }


}

module.exports = { LogsListener };