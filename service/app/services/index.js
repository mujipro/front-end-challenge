const { AgentService } = require('./agent');
const { CallService } = require('./call');
const { LogsService } = require('./logs');


module.exports = {
    AgentService,
    CallService,
    LogsService
};