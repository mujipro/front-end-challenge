const { AgentNotFound } = require('./agent');
const { CallNotFound } = require('./call');
const { LogsNotFound } = require('./logs');


module.exports = {
     AgentNotFound,
     CallNotFound,
     LogsNotFound
};