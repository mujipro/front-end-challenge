import React, {Component} from "react";
import {Bar, BarChart, CartesianGrid, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis,} from "recharts";

const data = [
  {
    name: "JAN",
    sales: 5,
  },
  {
    name: "FEB",
    sales: 10,
  },
  {
    name: "MAR",
    sales: 3,
  },
  {
    name: "APR",
    sales: 16,
  },
  {
    name: "MAY",
    sales: 20,
  },
  {
    name: "JUN",
    sales: 30,
  },
  {
    name: "JUL",
    sales: 25,
  },
  {
    name: "AUG",
    sales: 18,
  },
  {
    name: "SEP",
    sales: 10,
  },
  {
    name: "OCT",
    sales: 25,
  },
  {
    name: "NOV",
    sales: 29,
  },
  {
    name: "DEC",
    sales: 30,
  },
];

export default class CustomBarChart extends Component {
  static jsfiddleUrl = "https://jsfiddle.net/alidingling/q4eonc12/";

  render() {
    return (
      <div style={{ width: "99%", height: 300 }}>
        <ResponsiveContainer>
          <BarChart
            data={data}
            margin={{
              top: 10,
              right: 30,
              left: 0,
              bottom: 0,
            }}
            barSize={16}
          >
            <XAxis
              dataKey="name"
              scale="point"
              padding={{ left: 10, right: 10 }}
            />
            <YAxis />
            <Tooltip />
            <Legend />
            <CartesianGrid strokeDasharray="3 3" />
            <Bar dataKey="sales" fill="#8884d8" background={{ fill: "#eee" }} />
          </BarChart>
        </ResponsiveContainer>
      </div>
    );
  }
}
