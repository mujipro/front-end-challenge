const fs = require('fs');
const path = require("path");
const File = require("../../../file");

const readFile = (type) => {

    if(type === 'logs'){
        let fileNameLogs = path.join(__dirname, `../../../${File.path}`, File[type])
        let dataLogs = fs.readFileSync(fileNameLogs);
        if (dataLogs) {
            return JSON.parse(dataLogs);
        }
    }else if(type === 'resolutions'){

        let fileNameResolution = path.join(__dirname, `../../../${File.path}`, File[type])
        let dataResolution = fs.readFileSync(fileNameResolution);
        let agentsResolution = JSON.parse(dataResolution);
        let parsedDataResolution = {}
        agentsResolution.forEach(element => {
            parsedDataResolution[element.identifier] = element.resolution
        });
        return parsedDataResolution;

    }else if(type === 'agents'){
        console.log("path.join(__dirname, `../${File.path}`, File[type])",path.join(__dirname, `../${File.path}`, File[type]));
        let fileNameAgents = path.join(__dirname, `../../../${File.path}`, File[type])
        let dataAgents = fs.readFileSync(fileNameAgents);
        console.log("dataAgents",dataAgents);
        let agentsData = JSON.parse(dataAgents);
        console.log("agentsData",agentsData);
        let parsedDataAgents = {}
        agentsData.forEach(element => {
            parsedDataAgents[element.identifier] = element
        });
        return parsedDataAgents;
    }


}
module.exports = { readFile };