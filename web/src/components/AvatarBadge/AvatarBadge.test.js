import {shallow} from "enzyme";
import React from "react";
import '../../setupTests'
import AvatarBadge  from "./AvatarBadge";
import {Avatar} from "@material-ui/core";

it("app badge renders without crashing", () => {
  shallow( <AvatarBadge
      overlap="circular"
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right"
      }}
      variant="dot"
  >
    <Avatar
        alt="Guest"
        src="https://png.pngtree.com/png-clipart/20190520/original/pngtree-vector-users-icon-png-image_4144740.jpg"
    />
  </AvatarBadge>);
});

