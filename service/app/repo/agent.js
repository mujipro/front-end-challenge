const {readFile} = require("../shared/functions");


class AgentRepo {

    constructor() {
    }

    getAll(type) {
        return readFile(type);
    }

    async get(id) {
        try {
            const result = []
            try {
                const resolution = readFile('resolutions');
                const logs = readFile('logs');
                logs.forEach(element => {
                    const date = new Date(element.dateTime);
                    if (element["agentIdentifier"] === id) {
                        result.push({
                            number: element.number,
                            dateTime: date.toLocaleString(),
                            resolution: resolution[element.identifier]
                        })
                    }
                });
                return result;
            } catch (e) {
                return null;
            }
        } catch (ex) {
            throw(ex);
        }
    }

    getAgentCalls() {
        try {
            let logs = readFile('logs');
            let customData = {}
            logs.forEach(element => {
                if (!customData[element.agentIdentifier]) {
                    customData[element.agentIdentifier] = [element]
                } else {
                    customData[element.agentIdentifier].push(element)
                }
            });


            let agent = readFile('agents');
            let result = [];
            for (const number in customData) {
                const object = customData[number];
                const last = object[object.length - 1];
                const _agent = agent[last.agentIdentifier];
                const data = {
                    id: _agent.identifier,
                    numberOfCall: object.length,
                    name: _agent.firstName + " " + _agent.lastName,
                };
                result.push(data);
            }

            return result;

        } catch (ex) {
            throw (ex);
        }
    }

}

module.exports = {AgentRepo};
