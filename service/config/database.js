const { config } = require("./index");
const env = config.get('env');
const { database, username, password, mysqlConf } = config.get('db');


module.exports = {
    [env]:{
        username,
        password,
        database,
        ...mysqlConf
    }
}