const { AgentRepo } = require('../repo');

class AgentService {
    constructor() {
        this.agentRepo = new AgentRepo();
    }


    async getAll(type) {
       return this.agentRepo.getAll(type)
    }

    get(id) {
        return this.agentRepo.get(id)
    }

    getAgentByCall(){
        return this.agentRepo.getAgentCalls()
    }


}

module.exports = { AgentService };
