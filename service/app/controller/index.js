const { AgentController } = require('./agents');
const { CallController } = require('./calls');
const { AuthenticationController } = require('./authentication');
const { LogsController } = require('./logs');

module.exports = {
    AuthenticationController,
    AgentController,
    CallController,
    LogsController
};