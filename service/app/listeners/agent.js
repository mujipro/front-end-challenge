const { log } = require('../shared/functions/log');

class AgentListener {

    constructor(controller) {
        controller.on('service', ()=>{});
        controller.on('beforeBootstrap', ()=>{});
    }


}

module.exports = { AgentListener };