const { CallController } = require('./../controller');
const { callValidation } = require('./../validations');
const { CallListener } = require('./../listeners');
const { authenticate } = require("../shared/functions");

class CallRouter {

    constructor(router) {


        this.callController = new CallController();
        this.callListener = new CallListener(this.callController);


        this.router = router;

        this.registerRoutes();


    }

    registerRoutes() {
        this.router.get("/api/v1/calls/", authenticate,this.callController.getCalls.bind(this.callController));
        this.router.get("/api/v1/call/:number", authenticate,  callValidation('default') ,this.callController.getCallByNumber.bind(this.callController));
    }

}


module.exports = { CallRouter };