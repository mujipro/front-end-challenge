class LogsNotFound extends Error {
    constructor(...args) { super(args) }
}

module.exports = {
    LogsNotFound
};
