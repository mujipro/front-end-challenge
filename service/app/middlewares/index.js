const { accessControl } = require('./access-control');
const { error } = require('./error');
const { notfound } = require('./notfound');



module.exports = {
    accessControl,
    error,
    notfound
};