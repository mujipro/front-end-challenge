const log = require('node-file-logger');

const log_options = {
    timeZone: 'Asia/Karachi',
    folderPath: './logs/',
    dateBasedFileNaming: true,
    fileNamePrefix: 'Task_LOG_',
    fileNameSuffix: '',
    fileNameExtension: '.log',
    dateFormat: 'YYYY-MM-DD',
    timeFormat: 'HH:mm:ss.SSS',
    logLevel: 'debug',
    onlyFileLogging: true
};

log.SetUserOptions(log_options);


module.exports = { log };
