const { agentValidation } = require('./agent');
const { callValidation } = require('./call');
const { logsValidation } = require('./logs');



module.exports = {
    agentValidation,
    callValidation,
    logsValidation
};