const { createError } = require('./create-error');
const { formParser } = require('./formparser');
const { codeGenerator } = require('./code-generator');
const { authenticate } = require('./authenticate');
const { readFile } = require('./readFile');





module.exports = {
    formParser,
    createError,
    codeGenerator,
    authenticate,
    readFile

};