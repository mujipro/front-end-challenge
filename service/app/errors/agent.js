class AgentNotFound extends Error {
    constructor(...args) { super(args) }
}

module.exports = {
    AgentNotFound
};
